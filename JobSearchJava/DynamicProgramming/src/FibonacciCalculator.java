/*
 * Calculate 40th Fibonacci number.
 */
public class FibonacciCalculator {
    static int GetNthFibonacciNumber(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }

        int[] table = new int[n];
        table[0] = table[1] = 1;

        for (int i = 2; i < n; i++) {
            table[i] = table[i - 1] + table[i - 2];
        }

        return table[n - 1];
    }

    public static void main(String[] args) {
        System.out.println(GetNthFibonacciNumber(40));
    }
}
