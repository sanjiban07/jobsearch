import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortMeetings {

    public static int maxEvents(List<Integer> arrival, List<Integer> duration) {
        if (arrival == null || duration == null)
        {
            return 0;
        }

        int N = arrival.size();
        List<Integer> departure = new ArrayList<Integer>();
        for (int i = 0; i < N; i++)
        {
            departure.add(i, arrival.get(i) + duration.get(i));
        }

        int max = 0;
        int count = 0;
        arrival.sort((o1, o2) -> o1.compareTo(o2));
        departure.sort((o1, o2) -> o1.compareTo(o2));
        for (int i = 0, j = 0; i < N && j < N; )
        {
            if (arrival.get(i) <= departure.get(j))
            {
                count++;
                i++;
                if (count > max)
                {
                    max = count;
                }
            }
            else
            {
                j++;
                count--;
            }
        }

        return max;
    }

    public static void main(String[] args) {
        List<Integer> arrival = new ArrayList<Integer>(Arrays.asList(1, 3, 3, 5, 7));
        List<Integer> duration = new ArrayList<Integer>(Arrays.asList(2, 2, 1, 2, 1));
        System.out.println(maxEvents(arrival, duration));
    }

}
