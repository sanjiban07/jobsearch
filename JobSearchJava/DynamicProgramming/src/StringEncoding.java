public class StringEncoding {
    public static String collapseString(String inputString) {
        if (inputString == null || inputString.length() == 0) {
            return "";
        }

        char[] inputStrArray = inputString.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        char lastChar = inputStrArray[0];
        int count = 1;
        for (int i = 1; i < inputStrArray.length; i ++) {
            if (lastChar == inputStrArray[i]) {
                count ++;
            } else {
                stringBuilder.append(count);
                stringBuilder.append(lastChar);
                lastChar = inputStrArray[i];
                count = 1;
            }
        }

        stringBuilder.append(count);
        stringBuilder.append(lastChar);
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        System.out.println(collapseString("t")); 
    }
}
