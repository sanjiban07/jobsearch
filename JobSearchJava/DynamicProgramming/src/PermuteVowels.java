import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PermuteVowels {

    public static boolean isVowel(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'; 
    }
    


    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        String [] words = line.split(" ");
        boolean printNone = true;
        for (int i = 0; i < words.length; i ++) {
            String word = words[i];
            int count = 0;
            boolean printWord = false;
            for (int pos = 1; pos < word.length() - 1; pos ++) {
                if (!isVowel(word.charAt(pos))) {
                    if (isVowel(word.charAt(pos - 1)) && isVowel(word.charAt(pos + 1))) {
                        count ++;
                        printWord = true;
                        printNone  = false;
                    }
                }
            }

            if (printWord) {
                System.out.println(word + "(" + count + ")");
            }
        }

        if (printNone) {
            System.out.println("None");
        }
    }
}
