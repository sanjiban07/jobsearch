﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GooglePreparation
{
    public class GooglePrep
    {
        public int maximalRectangle(List<List<int>> A)
        {
            int M = A.Count();
            int N = A[0].Count();
            int max = 0;
            for (int row = M - 1; row > -1; row--)
            {
                for (int col = N - 1; col > -1; col--)
                {
                    if (A[row][col] != 0)
                    {
                        if (row == M - 1 || col == N - 1)
                        {
                            max = 1;
                        }
                        else
                        {
                            A[row][col] = 1 + Math.Min(A[row + 1][col], Math.Min(A[row][col + 1], A[row + 1][col + 1]));
                            if (A[row][col] > max)
                            {
                                max = A[row][col];
                            }
                        }
                    }
                }
            }

            return max * max;
        }

        public int majorityElement(List<int> A)
        {
            int N = A.Count();
            var map = new Dictionary<int, int>();
            A.ForEach(i => {
                if (!map.ContainsKey(i))
                {
                    map[i] = 0;
                }

                map[i]++;
            });

            int count = N >> 1;
            foreach (var item in map)
            {
                if (item.Value > count)
                {
                    return item.Key;
                }
            }

            return -1;
        }

        public int canCompleteCircuit(List<int> gas, List<int> cost)
        {
            int sumGas = 0;
            int sumCost = 0;
            int start = 0;
            int tank = 0;
            for (int i = 0; i < gas.Count(); i++)
            {
                sumGas += gas[i];
                sumCost += cost[i];
                tank += gas[i] - cost[i];
                if (tank < 0)
                {
                    start = i + 1;
                    tank = 0;
                }
            }
            if (sumGas < sumCost)
            {
                return -1;
            }
            else
            {
                return start;
            }
        }
        public int numDistinct(string A, string B)
        {
            int start = 0;
            while (start < A.Length - B.Length)
            {
                for (int i = 0, j = 0; i < A.Length && j < B.Length; i++)
                {
                    if (A[i] != B[i])
                    {
                        break;
                    }

                    j++;
                }
            }

            return 0;
        }

    }
}
