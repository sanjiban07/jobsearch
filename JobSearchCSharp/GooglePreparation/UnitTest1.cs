using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace GooglePreparation
{
    [TestClass]
    public class UnitTest1
    {
        GooglePrep prep = new GooglePrep();
        [TestMethod]
        public void TestGasStation()
        {
            Assert.AreEqual(1, prep.canCompleteCircuit(new List<int> { 1, 2 }, new List<int> { 2, 1 }));
        }

        [TestMethod]
        public void TestMaximalRectangle()
        {
            Assert.AreEqual(4, prep.maximalRectangle(
                new List<List<int>>()
                {
                    new List<int>() { 1, 1, 1},
                    new List<int>() { 0, 1, 1},
                    new List<int>() { 1, 0, 0},
                })
            );
        }
    }
}
