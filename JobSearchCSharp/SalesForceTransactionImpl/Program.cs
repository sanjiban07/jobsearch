﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SalesForceTransactionImpl
{
    class Program
    {
        class Transaction
        {
            public Dictionary<string, object> CurrentTransaction { get; private set; }
            public Dictionary<string, object> PreviousTransaction { get; private set; }

            public Transaction()
            {
                CurrentTransaction = new Dictionary<string, object>();
                PreviousTransaction = null;
                CurrentTransaction["Old Transaction"] = null;
            }

            public bool TryStartTransaction()
            {
                PreviousTransaction = CurrentTransaction;
                CurrentTransaction = new Dictionary<string, object>();
                CurrentTransaction["Old Transaction"] = PreviousTransaction;
                return true;
            }

            public bool TryEndTransaction()
            {
                if (PreviousTransaction == null || !CurrentTransaction.ContainsKey("Old Transaction"))
                {
                    return false;
                }
                else
                {
                    CurrentTransaction = PreviousTransaction;
                    PreviousTransaction = (Dictionary<string, object>)CurrentTransaction["Old Transaction"];
                    return true;
                }
            }
        }

        class CLICommandExecutionService
        {
            public enum ValidCommands
            {
                SET, GET, DELETE, COUNT, START_TRANSACTION, END_TRANSACTION, EXIT
            }

            public string Execute()
            {
                switch (Cmd)
                {
                    case ValidCommands.SET:
                        if (Args.Length != 2)
                        {
                            return "Invalid Arguments";
                        }
                        Transaction.CurrentTransaction[Args[0]] = Args[1];
                        return "SUCCESS";
                    case ValidCommands.GET:
                        if (Args.Length != 1)
                        {
                            return "Invalid Arguments";
                        }
                        if (Transaction.CurrentTransaction.ContainsKey(Args[0]))
                        {
                            return Transaction.CurrentTransaction[Args[0]].ToString();
                        }

                        return "Does not exist!!";
                    case ValidCommands.DELETE:
                        if (Args.Length != 1)
                        {
                            return "Invalid Arguments";
                        }
                        if (Transaction.CurrentTransaction.ContainsKey(Args[0]))
                        {
                            Transaction.CurrentTransaction.Remove(Args[0]);
                            return "SUCCESS";
                        }

                        return "Does not exist!!";
                    case ValidCommands.COUNT:
                        if (Args.Length != 1)
                        {
                            return "Invalid Arguments";
                        }

                        return Transaction.CurrentTransaction.Values.Where(val => val.Equals(Args[0])).Count().ToString();
                    case ValidCommands.START_TRANSACTION:
                        return Transaction.TryStartTransaction() ? "SUCCESS" : "FAILED";
                    case ValidCommands.END_TRANSACTION:
                        return Transaction.TryEndTransaction() ? "SUCCESS" : "FAILED";
                    case ValidCommands.EXIT:
                        return "EXIT";
                }

                return string.Empty;
            }

            public CLICommandExecutionService(Transaction transaction, string input)
            {
                if (input.Equals("START TRANSACTION"))
                    Cmd = ValidCommands.START_TRANSACTION;
                else if (input.Equals("END TRANSACTION"))
                {
                    Cmd = ValidCommands.END_TRANSACTION;
                }
                else
                {
                    string[] inputArr = input.Split(' ');
                    string cmd = inputArr[0];
                    if (!Enum.TryParse<ValidCommands>(cmd, out Cmd))
                    {
                        throw new ArgumentException($"Invalid Command '{cmd}'.");
                    }

                    Args = inputArr.Skip(1).ToArray();
                }

                Transaction = transaction;
            }

            public override string ToString()
            {
                return $"Cmd: {Cmd}, Args: {string.Join(", ", Args)}";
            }

            private readonly string[] Args;
            private readonly ValidCommands Cmd;
            private readonly Transaction Transaction;
        }

        class CLIController
        {
            public CLIController()
            {
                Transaction = new Transaction();
            }

            public string Execute(string input)
            {
                if (string.IsNullOrWhiteSpace(input))
                {
                    return "Empty Line!!";
                }

                return new CLICommandExecutionService(Transaction, input).Execute();
            }

            private Transaction Transaction;
        }

        public class Solution
        {
            public static void Main(string[] args)
            {
                /* Enter your code here. Read input from STDIN. Print output to STDOUT */
                var cliController = new CLIController();
                while (true)
                {
                    string output = cliController.Execute(Console.ReadLine());
                    Console.WriteLine(output);
                    if (output.Equals("EXIT"))
                    {
                        break;
                    }
                }
            }
        }
    }
}
