﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CommandProcessor
{
    public enum ValidCommands
    {
        QUIT,
        PWD,
        CD,
        TOUCH,
        LS,
        MKDIR,
    }

    public enum CommandOutput
    {
        SUCCESS,
        FAILED,
        INVALID,
    }

    public static class ErrorMsg
    {
        public const string INVALID_CMD = "Invalid command.";
        public const string INVALID_PARAMS = "Invalid parameters.";
        public const string MISSING_PARAMS = "Any parameter is missing.";
        public const string FILE_EXISTS = "File already exists.";
        public const string DIRECTORY_EXISTS = "Directory already exists.";
        public const string DIRECTORY_NOT_FOUND = "Directory not found.";
        public const string INVALID_NAME = "Invalid file or folder name.";
    }

    public abstract class ACommandExecutor
    {
        public virtual bool IsValid(ValidCommands cmd, string[] args)
        {
            return _validCommands.Contains(cmd);
        }

        public abstract CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output);

        public ValidCommands[] _validCommands;
    }

    public class LSExecutor : ACommandExecutor
    {
        public LSExecutor()
        {
            _validCommands = new ValidCommands[]
            {
                ValidCommands.LS,
            };
        }

        public override CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output)
        {
            if (IsValid(cmd, args))
            {
                if (args.Length == 0)
                {
                    output = GetChildren(Directory.GetCurrentDirectory());
                    return CommandOutput.SUCCESS;
                }
                else if (args.Length > 1)
                {
                    output = ErrorMsg.INVALID_PARAMS;
                    return CommandOutput.FAILED;
                }
                else if (args[0].Equals("-r", StringComparison.InvariantCultureIgnoreCase))
                {
                    output = GetChildrenRecursive(Directory.GetCurrentDirectory());
                    return CommandOutput.SUCCESS;
                }
                else
                {
                    output = ErrorMsg.INVALID_PARAMS;
                    return CommandOutput.FAILED;
                }
            }
            else
            {
                output = ErrorMsg.INVALID_CMD;
                return CommandOutput.INVALID;
            }
        }

        public string GetChildrenRecursive(string cwd)
        {
            var childDirs = Directory.GetDirectories(cwd);
            string output = GetChildren(cwd);
            if (childDirs.Length == 0)
            {
                return output;
            }

            output = output + "\n" + string.Join("\n", childDirs.Select(childDir => GetChildrenRecursive(childDir)));
            return output;
        }

        public string GetChildren(string cwd)
        {
            var children = new List<string>();
            children.Add($"\n\tDirectory: {cwd}\n");
            children.AddRange(Directory.GetDirectories(cwd));
            children.AddRange(Directory.GetFiles(cwd));
            return string.Join("\n", children);
        }
    }

    public class TouchExecutor : ACommandExecutor
    {
        public TouchExecutor()
        {
            _validCommands = new ValidCommands[]
            {
                ValidCommands.TOUCH,
            };
        }

        public override CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output)
        {
            if (IsValid(cmd, args))
            {
                if (args.Length == 0)
                {
                    output = ErrorMsg.MISSING_PARAMS;
                    return CommandOutput.FAILED;
                }
                else if (args.Length > 1)
                {
                    output = ErrorMsg.INVALID_PARAMS;
                    return CommandOutput.FAILED;
                }
                else
                {
                    string fileName = args[0];
                    if (fileName.Length > 100)
                    {
                        output = ErrorMsg.INVALID_NAME;
                        return CommandOutput.FAILED;
                    }

                    string filePath = Path.Combine(Directory.GetCurrentDirectory(), fileName);
                    if (File.Exists(filePath))
                    {
                        output = ErrorMsg.FILE_EXISTS;
                        return CommandOutput.SUCCESS;
                    }

                    try
                    {
                        File.Create(filePath);
                        output = string.Empty;
                        return CommandOutput.SUCCESS;
                    }
                    catch (Exception ex)
                    {
                        output = ex.Message;
                        return CommandOutput.FAILED;
                    }
                }
            }
            else
            {
                output = ErrorMsg.INVALID_CMD;
                return CommandOutput.INVALID;
            }
        }
    }

    public class MkdirExecutor : ACommandExecutor
    {
        public MkdirExecutor()
        {
            _validCommands = new ValidCommands[]
            {
                ValidCommands.MKDIR,
            };
        }

        public override CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output)
        {
            if (IsValid(cmd, args))
            {
                if (args.Length == 0)
                {
                    output = ErrorMsg.MISSING_PARAMS;
                    return CommandOutput.FAILED;
                }
                else if (args.Length > 1)
                {
                    output = ErrorMsg.INVALID_PARAMS;
                    return CommandOutput.FAILED;
                }
                else
                {
                    string dirName = args[0];
                    if (dirName.Length > 100)
                    {
                        output = ErrorMsg.INVALID_NAME;
                        return CommandOutput.FAILED;
                    }

                    string dirPath = Path.Combine(Directory.GetCurrentDirectory(), dirName);
                    if (Directory.Exists(dirPath))
                    {
                        output = ErrorMsg.DIRECTORY_EXISTS;
                        return CommandOutput.SUCCESS;
                    }

                    try
                    {
                        Directory.CreateDirectory(dirPath);
                        output = string.Empty;
                        return CommandOutput.SUCCESS;
                    }
                    catch (Exception ex)
                    {
                        output = ex.Message;
                        return CommandOutput.FAILED;
                    }
                }
            }
            else
            {
                output = ErrorMsg.INVALID_CMD;
                return CommandOutput.INVALID;
            }
        }
    }

    public class CDExecutor : ACommandExecutor
    {
        public CDExecutor()
        {
            _validCommands = new ValidCommands[]
            {
                ValidCommands.CD
            };
        }

        public override CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output)
        {
            if (IsValid(cmd, args))
            {
                if (args.Length == 0)
                {
                    output = ErrorMsg.MISSING_PARAMS;
                    return CommandOutput.FAILED;
                }
                else if (args.Length > 1)
                {
                    output = ErrorMsg.INVALID_PARAMS;
                    return CommandOutput.FAILED;
                }
                else
                {
                    string dir = args[0];
                    if (dir.Equals(".."))
                    {
                        var parent = Directory.GetParent(Directory.GetCurrentDirectory());
                        if (parent == null)
                        {
                            dir = Directory.GetCurrentDirectory();
                        }
                        else
                        {
                            dir = parent.FullName;
                        }
                    }

                    try
                    {
                        Directory.SetCurrentDirectory(dir);
                        output = string.Empty;
                        return CommandOutput.SUCCESS;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        output = ErrorMsg.DIRECTORY_NOT_FOUND;
                        return CommandOutput.FAILED;
                    }
                }
            }
            else
            {
                output = ErrorMsg.INVALID_CMD;
                return CommandOutput.INVALID;
            }
        }
    }

    public class PWDExecutor : ACommandExecutor
    {
        public PWDExecutor()
        {
            _validCommands = new ValidCommands[]
            {
                ValidCommands.PWD
            };
        }

        public override CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output)
        {
            if (IsValid(cmd, args))
            {
                if (args.Length > 0)
                {
                    output = ErrorMsg.INVALID_PARAMS;
                    return CommandOutput.FAILED;
                }
                else
                {
                    output = Directory.GetCurrentDirectory();
                    return CommandOutput.SUCCESS;
                }
            }
            else
            {
                output = ErrorMsg.INVALID_CMD;
                return CommandOutput.INVALID;
            }
        }
    }

    public class BasicCommandExecutor : ACommandExecutor
    {
        List<ACommandExecutor> commandExecutors;

        public BasicCommandExecutor()
        {
            commandExecutors = new List<ACommandExecutor>()
            {
                new PWDExecutor(),
                new CDExecutor(),
                new TouchExecutor(),
                new LSExecutor(),
                new MkdirExecutor(),
            };
        }

        public override bool IsValid(ValidCommands cmd, string[] args)
        {
            return commandExecutors.Any(ex => ex.IsValid(cmd, args));
        }

        public override CommandOutput TryExecute(ValidCommands cmd, string[] args, out string output)
        {
            var executor = commandExecutors.FirstOrDefault(ex => ex.IsValid(cmd, args));
            if (executor == null)
            {
                output = ErrorMsg.INVALID_CMD;
                return CommandOutput.INVALID;
            }

            return executor.TryExecute(cmd, args, out output);
        }
    }

    public class CommandProcessor
    {
        public CommandProcessor()
        {
            _commandExecutor = new BasicCommandExecutor();
        }

        public bool Execute(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Empty Line!!");
                return true;
            }

            input = input.Trim();
            string[] inputArr = input.Split(' ');
            string cmd = inputArr[0];
            string[] args = inputArr.Skip(1).ToArray();
            if (!Enum.TryParse(cmd, true, out ValidCommands Cmd))
            {
                Console.WriteLine(ErrorMsg.INVALID_CMD);
                return true;
            }
            else if (Cmd == ValidCommands.QUIT)
            {
                return false;
            }
            else
            {
                _commandExecutor.TryExecute(Cmd, args, out string output);
                if (!string.IsNullOrWhiteSpace(output))
                {
                    Console.WriteLine(output);
                }
                return true;
            }
        }

        ACommandExecutor _commandExecutor;
    }

    public class Program
    {
        static void Main(string[] args)
        {
            CommandProcessor commandProcessor = new CommandProcessor();
            bool doNotExit = true;
            do
            {
                Console.Write($"{Directory.GetCurrentDirectory()} $> ");
                string input = Console.ReadLine();
                doNotExit = commandProcessor.Execute(input);
            } while (doNotExit);
        }
    }
}
