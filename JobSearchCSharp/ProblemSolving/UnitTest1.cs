using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProblemSolving.Programs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ProblemSolving
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void BitapSearchTest()
        {
            Assert.AreEqual(9, BitapSearch.StringSearch("archaeopteryx", "eryx", 2));
        }

        [TestMethod]
        public void PermuteTwoArraysTest()
        {
            Assert.AreEqual("YES", PermuteTwoArrays.TwoArrays(10, new int[] { 2, 1, 3 }, new int[] { 7, 8, 9 }));
            Assert.AreEqual("NO", PermuteTwoArrays.TwoArrays(5, new int[] { 1, 2, 2, 1 }, new int[] { 3, 3, 3, 4 }));
        }

        [TestMethod]
        public void CalculateAmountTest()
        {
            Assert.AreEqual(10, MarketDiscount.CalculateAmount(new int[] { 4, 9, 2, 3 }.ToList()));
        }

        [TestMethod]
        public void CollapseStringTest()
        {
            Assert.AreEqual("5G14r1t", CollapseString("GGGGGrrrrrrrrrrrrrrt"));
        }

        public static string CollapseString(string inputString)
        {
            if (inputString == null || inputString.Length == 0)
            {
                return "";
            }

            char[] inputStrArray = inputString.ToCharArray();
            StringBuilder stringBuilder = new StringBuilder();
            char lastChar = inputStrArray[0];
            int count = 1;
            for (int i = 1; i < inputStrArray.Length; i++)
            {
                if (lastChar == inputStrArray[i])
                {
                    count++;
                }
                else
                {
                    stringBuilder.Append(count);
                    stringBuilder.Append(lastChar);
                    lastChar = inputStrArray[i];
                    count = 1;
                }
            }

            stringBuilder.Append(count);
            stringBuilder.Append(lastChar);
            return stringBuilder.ToString();
        }


        [TestMethod]
        public void TestMaxEvents()
        {
            Assert.AreEqual(3, MaxSimultaneousEvents.MaxEvents(new List<int>() { 1, 3, 3, 5, 7 }, new List<int>() { 2, 2, 1, 2, 1 }));
        }

        class Element
        {
            public int value;
            public int idx;
        }

        // Complete the largestPermutation function below.
        static int[] largestPermutation(int k, int[] arr)
        {
            if (k == 0 || arr == null || arr.Length < 2)
            {
                return arr;
            }

            int i, j;
            var elems = new Element[arr.Length];
            for (i = 0; i < arr.Length; i++)
            {
                elems[i] = new Element()
                {
                    value = arr[i],
                    idx = i,
                };
            }

            i = 0; j = 0;
            k = k > arr.Length? arr.Length : k;
            elems = elems.OrderByDescending(elem => elem.value).ToArray();
            while (i<k && j < arr.Length) {
                if (arr[i] >= elems[j].value) {
                    i++;
                    j++;
                } else {
                    int temp = arr[i];
                    arr[i] = elems[j].value;
                    arr[elems[j].idx] = temp;
                }
            }

            return arr;
        }

        [TestMethod]
        public void TestClosesDistance()
        {
            var x = new List<int>() { 77, 1000, 992, 1000000 };
            var y = new List<int>() { 0, 1000, 500, 0 };
            Assert.AreEqual(250064, closestSquaredDistance(x, y));
        }

        class Point
        {
            public int x;
            public int y;
        }

        // Complete the closestSquaredDistance function below.
        static long closestSquaredDistance(List<int> x, List<int> y)
        {
            int N = x.Count();
            long distSqr, min = long.MaxValue;
            var points = new Point[N];
            for (int i = 0; i < N; i++)
            {
                points[i] = new Point()
                {
                    x = x[i],
                    y = y[i],
                };
            }
            return min;
        }

        [TestMethod]
        public void TestFinalPrice()
        {
            Assert.AreEqual(8, finalPrice(new List<int>() { 5, 1, 3, 4, 6, 2 }));
        }

        public static long finalPrice(List<int> prices)
        {
            if (prices == null || prices.Count() == 0)
            {
                Console.WriteLine("0");
                Console.WriteLine("0 0");
                return 0;
            }

            int N = prices.Count(), discount = 0;
            long total = prices[N - 1];
            var minArr = new int[N];
            var noDiscount = new List<int>();
            noDiscount.Add(prices[N - 1]);
            minArr[N - 1] = -1;
            for (int i = N - 2; i >= 0; i--)
            {
                int j = i + 1;
                while (j < N)
                {
                    if (prices[i] >= prices[j])
                    {
                        minArr[i] = j;
                        discount = prices[j];
                        break;
                    }
                    else
                    {
                        if (minArr[j] == -1)
                        {
                            minArr[i] = -1;
                            discount = 0;
                            break;
                        }
                        else
                        {
                            j = minArr[j];
                        }
                    }
                }

                total += prices[i] - discount;
                if (discount == 0)
                {
                    noDiscount.Add(prices[i]);
                }
            }

            Console.WriteLine(total);
            Console.WriteLine(string.Join(' ', noDiscount));
            return total;
        }


        public static string isPossible(int a, int b, int c, int d)
        {
            if (a > c || b > d)
            {
                return "No";
            }

            var queue = new Queue<Tuple<int, int>>();
            queue.Enqueue(new Tuple<int, int>(a, b));
            while (queue.Any())
            {
                var item = queue.Dequeue();
                int x = item.Item1, y = item.Item2;
                if (x == c && y == d)
                {
                    return "Yes";
                }

                if (x + y <= c)
                {
                    queue.Enqueue(new Tuple<int, int>(x + y, y));
                }

                if (x + y <= d)
                {
                    queue.Enqueue(new Tuple<int, int>(x, x + y));
                }
            }

            return "No";
        }

        // Complete the decentNumber function below.
        static void decentNumber(int n)
        {
            for (int i = 3 * (n/3); i >= 0; i = i - 3)
            {
                if (i % 3 == 0 && (n - i) % 5 == 0)
                {
                    for (int j = 0; j < i; j++)
                    {
                        Console.Write("5");
                    }

                    for (int j = 0; j < n-i; j++)
                    {
                        Console.Write("3");
                    }

                    Console.Write("\n");
                }
            }

            Console.WriteLine("-1");
        }

    }
}
