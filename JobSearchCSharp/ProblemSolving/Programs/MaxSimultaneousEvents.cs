﻿using System.Collections.Generic;
using System.Linq;

namespace ProblemSolving.Programs
{
    public static class MaxSimultaneousEvents
    {
        public static int MaxEvents(List<int> arrival, List<int> duration)
        {
            if (arrival == null || duration == null || arrival.Count() == 0 || duration.Count() == 0)
            {
                return 0;
            }

            int N = arrival.Count();
            List<int> departure = new List<int>();
            for (int i = 0; i < N; i++)
            {
                departure.Add(arrival[i] + duration[i]);
            }

            int max = 0;
            int count = 0;
            arrival = arrival.OrderBy(i => i).ToList();
            departure = departure.OrderBy(i => i).ToList();
            for (int i = 0, j = 0; i < N && j < N;)
            {
                if (arrival[i] <= departure[j])
                {
                    count++;
                    i++;
                    if (count > max)
                    {
                        max = count;
                    }
                }
                else
                {
                    j++;
                    count--;
                }
            }
            return max;
        }
    }
}
