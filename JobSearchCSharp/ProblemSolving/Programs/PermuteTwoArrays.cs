﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProblemSolving.Programs
{
    public class PermuteTwoArrays
    {
        public static string TwoArrays(int k, int[] A, int[] B)
        {
            var A1 = A.OrderBy(a => a).ToArray();
            var B1 = B.OrderByDescending(b => b).ToArray();
            for (int i = 0; i < A.Length; i ++)
            {
                if (A1[i] + B1[i] < k)
                {
                    return "NO";
                }
            }

            return "YES";
        }
    }
}
