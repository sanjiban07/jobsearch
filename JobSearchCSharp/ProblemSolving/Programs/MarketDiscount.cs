﻿using System.Collections.Generic;

namespace ProblemSolving.Programs
{
    public class MarketDiscount
    {
        public static long CalculateAmount(List<int> prices)
        {
            if (prices == null || prices.Count == 0)
            {
                return 0;
            }

            long amount = prices[0];
            int minPrice = prices[0];
            for (int i = 1; i < prices.Count; i++)
            {
                amount += prices[i] > minPrice ? prices[i] - minPrice : 0;
                if (prices[i] < minPrice)
                {
                    minPrice = prices[i];
                }
            }

            return amount;
        }
    }
}
