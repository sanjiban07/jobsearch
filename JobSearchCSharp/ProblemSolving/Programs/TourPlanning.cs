﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.Programs
{
    class TourPlanning
    {
        class PathInfo
        {
            public int Loc { get; private set; }
            public int Beauty { get; private set; }
            public int Cost { get; private set; }
            public bool[] VisitedLoc { get; }

            public PathInfo(int loc, int beauty, int cost, bool[] visitedLoc)
            {
                Loc = loc;
                Beauty = beauty;
                Cost = cost;
                VisitedLoc = visitedLoc;
                VisitedLoc[Loc] = true;
            }

            public PathInfo(PathInfo pathInfo)
            {
                Loc = pathInfo.Loc;
                Beauty = pathInfo.Beauty;
                Cost = pathInfo.Cost;
                VisitedLoc = new bool[pathInfo.VisitedLoc.Length];
                for (int i = 0; i < VisitedLoc.Length; i++)
                {
                    VisitedLoc[i] = pathInfo.VisitedLoc[i];
                }
            }

            public void Move(int nextLoc, int cost, int beauty)
            {
                Loc = nextLoc;
                Cost += cost;
                if (!VisitedLoc[Loc])
                {
                    VisitedLoc[Loc] = true;
                    Beauty = Beauty + beauty;
                }
            }
        }

        public static int findBestPath(int n, int m, int max_t, List<int> beauty, List<int> u, List<int> v, List<int> t)
        {
            int[,] edgeGraph = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    edgeGraph[i, j] = -1;
                }
            }

            for (int i = 0; i < m; i++)
            {
                edgeGraph[u[i], v[i]] = t[i];
                edgeGraph[v[i], u[i]] = t[i];
            }

            int maxBeauty = beauty[0];
            var visitedLocations = new Queue<PathInfo>();
            var visitedLoc = new bool[n];
            visitedLoc[0] = true;
            PathInfo pathInfo = new PathInfo(0, beauty[0], 0, visitedLoc);
            while (visitedLocations.Count > 0)
            {
                var currentPathInfo = visitedLocations.Dequeue();
                if (currentPathInfo.Loc == 0 && currentPathInfo.Beauty > maxBeauty)
                {
                    maxBeauty = currentPathInfo.Beauty;
                }

                for (int i = 0; i < n; i++)
                {
                    if (edgeGraph[currentPathInfo.Loc, i] > 0 && edgeGraph[currentPathInfo.Loc, i] + currentPathInfo.Cost <= max_t)
                    {
                        PathInfo newPath = new PathInfo(currentPathInfo);
                        newPath.Move(i, edgeGraph[currentPathInfo.Loc, i], beauty[i]);
                        edgeGraph[currentPathInfo.Loc, i] = -1;
                    }
                }
            }

            return maxBeauty;
        }
    }
}
