﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.Programs
{
    class MatrixExplorer
    {
        public static string canReach(int x1, int y1, int x2, int y2)
        {
            var pairs = new Queue<Tuple<int, int>>();
            pairs.Enqueue(new Tuple<int, int>(x1, y1));
            while (pairs.Count > 0)
            {
                Tuple<int, int> pair = pairs.Dequeue();
                int key = pair.Item1;
                int value = pair.Item2;
                if (key == x1 && value == y1)
                {
                    return "YES";
                }
                int sum = key + value;
                if (sum <= x2)
                {
                    pairs.Enqueue(new Tuple<int, int>(sum, value));
                }
                if (sum <= y2)
                {
                    pairs.Enqueue(new Tuple<int, int>(key, sum));
                }
            }

            return "NO";
        }
    }
}
