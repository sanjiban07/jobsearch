﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProblemSolving.Programs
{
    class VowelPermutation
    {
        public static int[] nextPos(int x)
        {
            switch (x)
            {
                case 0:
                    return new int[] { 1 };
                case 1:
                    return new int[] { 0, 2 };
                case 2:
                    return new int[] { 0, 1, 3, 4 };
                case 3:
                    return new int[] { 2, 4 };
                default:
                    return new int[] { 0 };
            }
        }

        public static int countPerms(int n)
        {
            if (n <= 0)
            {
                return 0;
            }

            if (n == 1)
            {
                return 5;
            }

            int[,] dp = new int[5, n];
            for (int row = 0; row < 5; row++)
            {
                dp[row, 0] = 1;
            }

            for (int col = 1; col < n; col++)
            {
                for (int row = 0; row < 5; row++)
                {
                    var update = nextPos(row);
                    foreach (int x in update)
                    {
                        dp[x, col] += dp[row, col - 1];
                    }
                }
            }

            int total = 0;
            for (int row = 0; row < 5; row++)
            {
                total += dp[row, n - 1];
            }
            return total;
        }
    }
}
