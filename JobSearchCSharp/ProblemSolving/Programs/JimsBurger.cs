﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProblemSolving.Programs
{
    public static class JimsBurger
    {
        class Order
        {
            public int CustomerId;
            public int OrderId;
            public int PrepTime;
            public int ServeTime;
            public Order(int customerId, int orderId, int prepTime)
            {
                CustomerId = customerId;
                OrderId = orderId;
                PrepTime = prepTime;
                ServeTime = OrderId + PrepTime;
            }
        }

        // Complete the jimOrders function below.
        public static int[] JimOrders(int[][] orders)
        {
            var orderList = new List<Order>();
            for (int i = 0; i < orders.Length; i ++)
            {
                orderList.Add(new Order(i, orders[i][0], orders[i][1]));
            }

            return orderList.OrderBy(order => order.ServeTime).Select(order => order.CustomerId).ToArray();
        }
    }
}
