﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace ProblemSolving
{
    [TestClass]
    public class SecureAI
    {
        public class Page
        {
            public int page; // page: The current page.
            public int per_page;//The maximum number of results per page.
            public int total;//The total number of records in the search result.
            public int total_pages;//The total number of pages which must be queried to get all the results.
            public Data[] data;
        }

        public class Data
        {
            public string Title;
            public string Year;
            public string imdbID;
        }

        static string[] getMovieTitles(string substr)
        {
            var titles = new List<string>();
            var httpClient = new HttpClient();
            for (int pageCnt = 1; true; pageCnt++)
            {
                var content = httpClient.GetStringAsync($"https://jsonmock.hackerrank.com/api/movies/search/?Title={substr}&page={pageCnt}").Result;
                Page page = JsonConvert.DeserializeObject<Page>(content);
                titles.AddRange(page.data.Select(elem => elem.Title));
                if (page.page >= page.total_pages)
                {
                    break;
                }
            }

            return titles.OrderBy(title => title).ToArray();
        }

        static string leftrotate(string str, int d)
        {
            return str.Substring(d) + str.Substring(0, d);
        }

        static string rightrotate(string str, int d)
        {
            return leftrotate(str, str.Length - d);
        }

        public static string getShiftedString(string s, int leftShifts, int rightShifts)
        {
            if (s == null)
            {
                return s;
            }

            int N = s.Length;
            return rightrotate(leftrotate(s, leftShifts % N), rightShifts % N);
        }

        [TestMethod]
        public void TestShiftedString()
        {
            Assert.AreEqual("dabc", getShiftedString("abcd", 1, 2));
            Assert.AreEqual("abcd", getShiftedString("abcd", 4, 0));
            Assert.AreEqual("abcd", getShiftedString("abcd", 0, 4));
            Assert.AreEqual("eksforGeeksGe", getShiftedString("GeeksforGeeks", 3, 1));
            Assert.AreEqual("ksGeeksforGee", getShiftedString("GeeksforGeeks", 1, 3));
        }
    }
}
