﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Commons
{
    public class PriorityQueue<T1, T2> 
        where T1 : IComparable
        where T2 : IComparable
    {
        public PriorityQueue()
        {
            keyList = new List<T1>();
            valueList = new List<T2>();
            maxSize = -1;
        }

        public PriorityQueue(int size)
        {
            keyList = new List<T1>(size);
            valueList = new List<T2>(size);
            maxSize = size;
        }

        public void EnQueue(T1 key, T2 value)
        {
            int N = keyList.Count();
            if (N == 0 && (maxSize > 0 || maxSize == -1))
            {
                keyList.Add(key);
                valueList.Add(value);
                return;
            }

            InsertAt(FindPos(key, value, N), key, value, N);
        }

        public T2 Dequeue()
        {
            int N = keyList.Count();
            if (N == 0)
            {
                return default;
            }

            T2 value = valueList[N - 1];
            valueList.RemoveAt(N - 1);
            keyList.RemoveAt(N - 1);
            return value;
        }

        public List<T2> GetAllValues()
        {
            return valueList;
        }

        public override string ToString()
        {
            string ret = string.Empty;
            for (int i = 0; i < keyList.Count; i++)
            {
                ret += $"{keyList[i]}, {valueList[i]}\n";
            }

            return ret;
        }

        private int FindPos(T1 key, T2 value, int N)
        {
            int left = 0;
            int right = N - 1;
            int mid;

            while (left <= right)
            {
                mid = (left + right) / 2;
                if (keyList[mid].CompareTo(key) > 0)
                {
                    left = mid + 1;
                }
                else if (keyList[mid].CompareTo(key) < 0)
                {
                    right = mid - 1;
                }
                else if (valueList[mid].CompareTo(value) > 0)
                {
                    left = mid + 1;
                }
                else if (valueList[mid].CompareTo(value) < 0)
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid;
                }
            }

            return left;
        }

        private void InsertAt(int pos, T1 key, T2 value, int N)
        {
            if (pos >= maxSize)
            {
                return;
            }

            if (maxSize == -1 || N < maxSize)
            {
                keyList.Add(default);
                valueList.Add(default);
                N = N + 1;
            }

            for (int i = N - 1; i > pos; i--)
            {
                keyList[i] = keyList[i-1];
                valueList[i] = valueList[i-1];
            }

            keyList[pos] = key;
            valueList[pos] = value;
        }

        private List<T1> keyList;
        private List<T2> valueList;
        private int maxSize;
    }
}
