﻿using System;
using System.Text;

namespace TestApp
{
    /// <summary>
    /// Maze class responsible for solving maze.
    /// There should be atleast one path from (0, 0) to (RowCnt - 1, ColCnt - 1)
    /// </summary>
    /// <see cref="https://en.wikipedia.org/wiki/Maze_solving_algorithm#Wall_follower"/>
    /// <see cref="https://www.youtube.com/watch?v=br5nBcy5MAo"/>
    class Maze
    {
        #region Constructor

        /// <summary>
        /// Initialize maze into a jagged array.
        /// </summary>
        /// <param name="maze">Given 2D array</param>
        public Maze(int[,] maze)
        {
            // Assuming user always gives correct matrix.
            RowCnt = maze.GetLength(0);
            ColCnt = maze.GetLength(1);
            this.maze = new int[RowCnt][];
            for (int row = 0; row < RowCnt; row++)
            {
                this.maze[row] = new int[ColCnt];
                for (int col = 0; col < ColCnt; col++)
                {
                    this.maze[row][col] = maze[row, col];
                }
            }
        }

        #endregion Constructor


        #region Methods

        /// <summary>
        /// Prints path from (0, 0) to (RowCnt - 1, ColCnt - 1) using backtracking.
        /// </summary>
        public void Path()
        {
            int row = 0, col = 0;
            int nextRow, nextCol;
            int prevRow = 0, prevCol = -1;
            Console.WriteLine($"({row}, {col})");
            while (row != RowCnt - 1 || col != ColCnt - 1)
            {
                NextPos(prevRow, prevCol, row, col, out nextRow, out nextCol);
                if (IsValid(nextRow, nextCol))
                {
                    prevRow = row;
                    prevCol = col;
                    row = nextRow;
                    col = nextCol;
                    Console.WriteLine($"({row}, {col})");
                }
                else
                {
                    prevRow = nextRow;
                    prevCol = nextCol;
                }
            }
        }

        /// <summary>
        /// Prints the maze.
        /// </summary>
        /// <returns>Generated string</returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < maze.Length; i++)
            {
                builder.Append(string.Join(" ", maze[i]) + "\n");
            }

            return builder.ToString();
        }

        /// <summary>
        /// Find next position
        /// </summary>
        /// <param name="prevRow">Previous row</param>
        /// <param name="prevCol">Previous column</param>
        /// <param name="row">Current row</param>
        /// <param name="col">Current column</param>
        /// <param name="nextRow">Next row</param>
        /// <param name="nextCol">Next column</param>
        public void NextPos(int prevRow, int prevCol, int row, int col, out int nextRow, out int nextCol)
        {
            if (prevRow == row)
            {
                if (prevCol + 1 == col)
                {// Previous movement was from left to right so, go up.
                    nextRow = row - 1;
                    nextCol = col;
                }
                else
                {// Previous movement was from right to left so, go down.
                    nextRow = row + 1;
                    nextCol = col;
                }
            }
            else
            {
                if (prevRow + 1 == row)
                {// Previous movement was from bottom to up so, go right.
                    nextRow = row;
                    nextCol = col + 1;
                }
                else
                {// Previous movement was from up to down so, go left.
                    nextRow = row;
                    nextCol = col - 1;
                }
            }
        }

        /// <summary>
        /// Returns true if its possible to move into this position.
        /// </summary>
        /// <param name="row">Given row</param>
        /// <param name="col">Givel column</param>
        /// <returns>True if its a valid position</returns>
        private bool IsValid(int row, int col)
        {
            if (row < 0 || row >= RowCnt)
            {
                return false;
            }
            else if (col < 0 || col >= ColCnt)
            {
                return false;
            }
            else if (maze[row][col] == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion Methods

        #region Fields

        private readonly int[][] maze;
        private readonly int RowCnt, ColCnt;

        #endregion Fields
    }

    /// <summary>
    /// Test class for testing MazeSolver
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            Maze maze = new Maze(
                new int[,] {
                    {1, 1, 1, 0},
                    {1, 0, 0, 0},
                    {1, 1, 1, 0},
                    {0, 0, 1, 0},
                    {0, 1, 1, 1}
                }
            );

            Console.WriteLine("Maze:");
            Console.WriteLine(maze);
            Console.WriteLine("Path:");
            maze.Path();
        }
    }
}
