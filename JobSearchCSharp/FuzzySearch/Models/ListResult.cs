﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuzzySearch.Models
{
    public class ListResult<T>
    {
        public IEnumerable<T> Result { get; set; }
        public int Count { get; set; }
        public TimeSpan timeConsumed { get; set; }

        public ListResult(IEnumerable<T> result)
        {
            Result = result ?? throw new ArgumentNullException("Null result");
            Count = Result.Count();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}