﻿using System;
using System.Net.Http.Headers;

namespace FuzzySearch.Models
{
    public class DictionaryWord : IComparable
    {
        public string Word { get; set; }
        public long TimesUsed { get; set; }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return TimesUsed.CompareTo(((DictionaryWord)obj).TimesUsed);
        }
    }
}