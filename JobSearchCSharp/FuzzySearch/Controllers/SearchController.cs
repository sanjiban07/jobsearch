﻿using FuzzySearch.DAL;
using FuzzySearch.Models;
using FuzzySearch.Services.Interfaces;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace FuzzySearch.Controllers
{
    public class SearchController : ApiController
    {
        public SearchController()
        {
            wordDictionary = new WordDictionary($"{AppContext.BaseDirectory}\\Data\\word_search.tsv");
            autoCompleteService = new Services.FuzzySearchImpl();
        }

        // GET api/search
        public HttpResponseMessage Get(string word, int size)
        {
            var watch = Stopwatch.StartNew();
            var result = new ListResult<string>(autoCompleteService.Filter(wordDictionary.Dictionary, word, size).Select(item => item.Word));
            result.timeConsumed = watch.Elapsed;
            watch.Stop();
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(result.ToString(), Encoding.UTF8, "application/json");
            return response;
        }

        private IFuzzySearch autoCompleteService;
        private WordDictionary wordDictionary;
    }
}