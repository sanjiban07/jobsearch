﻿using FuzzySearch.DAL.Interfaces;
using FuzzySearch.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FuzzySearch.DAL
{
    public class WordDictionary : IWordDictionary
    {
        public List<DictionaryWord> Dictionary { get; }

        public WordDictionary(string path)
        {
            Dictionary = new List<DictionaryWord>();
            using (var reader = new StreamReader(path))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    var lineSplit = line.Split('\t');
                    if (lineSplit == null || lineSplit.Length != 2)
                    {
                        throw new InvalidDataException($"Invalid line '{line}'");
                    }
                    else if (long.TryParse(lineSplit[1], out long frequency))
                    {
                        Dictionary.Add(new DictionaryWord()
                        {
                            Word = lineSplit[0],
                            TimesUsed = frequency,
                        });
                    }
                    else
                    {
                        throw new InvalidCastException($"Invalid line '{line}'");
                    }
                }
            }
        }

        public IEnumerable<string> GetWords(int size)
        {
            return Dictionary.Take(size).Select(wd => wd.Word);
        }

    }
}