﻿using FuzzySearch.Models;
using System.Collections.Generic;

namespace FuzzySearch.DAL.Interfaces
{
    public interface IWordDictionary
    {
        List<DictionaryWord> Dictionary { get; }
        IEnumerable<string> GetWords(int size);
    }
}
