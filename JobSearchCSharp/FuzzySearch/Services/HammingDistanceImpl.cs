﻿using System;
using System.Linq;

namespace FuzzySearch.Services
{
    public static class HammingDistanceImpl
    {
        public static int GetDistance(string word1, string word2)
        {
            if (word1.Length != word2.Length)
            {
                throw new Exception("Strings must be equal length");
            }

            int distance =
                word1.ToCharArray()
                .Zip(word2.ToCharArray(), (c1, c2) => new { c1, c2 })
                .Count(m => m.c1 != m.c2);

            return distance;
        }
    }
}