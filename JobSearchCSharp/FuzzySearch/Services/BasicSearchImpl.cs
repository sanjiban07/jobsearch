﻿using Commons;
using FuzzySearch.DAL;
using FuzzySearch.DAL.Interfaces;
using FuzzySearch.Models;
using FuzzySearch.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace FuzzySearch.Services
{
    public class BasicSearchImpl : IFuzzySearch
    {
        public IEnumerable<DictionaryWord> Filter(IEnumerable<DictionaryWord> wordDictionary, string word, int size)
        {
            var pQueue = new PriorityQueue<double, DictionaryWord>(size);
            foreach (var item in wordDictionary)
            {
                pQueue.EnQueue(CompareTwoWord(word, item.Word), item);
            }

            return pQueue.GetAllValues();
        }

        private int GetHashedWord(string word)
        {
            int value = 0;
            word = word.ToLower();
            foreach (char ch in word)
            {
                value |= 1 << ((int)ch - (int)'a');
            }

            return value;
        }

        private int CountSetBits(int n)
        {
            int count = 0;
            while (n > 0)
            {
                count += n & 1;
                n >>= 1;
            }
            return count;
        }

        private double CompareTwoWord(string word1, string word2)
        {
            int hash1 = GetHashedWord(word1);
            int hash2 = GetHashedWord(word2);
            double value = (100.0 * CountSetBits(hash1 & hash2)) / CountSetBits(hash1 | hash2);
            return value;
        }
    }
}