﻿using Commons;
using FuzzySearch.Models;
using FuzzySearch.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace FuzzySearch.Services
{
    public class DamerauLevenshteinDistanceImpl : IFuzzySearch
    {
        public IEnumerable<DictionaryWord> Filter(IEnumerable<DictionaryWord> allWords, string word, int size)
        {
            var priorityQueue = new PriorityQueue<int, DictionaryWord>(size);
            foreach (var item in allWords)
            {
                int distance;
                if (word.Length < item.Word.Length)
                {
                    distance = Math.Min(
                        HammingDistanceImpl.GetDistance(word, item.Word.Substring(0, word.Length))
                        , GetDamerauLevenshteinDistance(item.Word, word));
                }
                else
                {
                    distance = GetDamerauLevenshteinDistance(item.Word, word);
                }

                priorityQueue.EnQueue(int.MaxValue - distance, item);
            }

            return priorityQueue.GetAllValues();
        }

        public static int GetDamerauLevenshteinDistance(string word1, string word2)
        {
            var bounds = new { Height = word1.Length + 1, Width = word2.Length + 1 };

            int[,] matrix = new int[bounds.Height, bounds.Width];

            for (int height = 0; height < bounds.Height; height++) { matrix[height, 0] = height; };
            for (int width = 0; width < bounds.Width; width++) { matrix[0, width] = width; };

            for (int height = 1; height < bounds.Height; height++)
            {
                for (int width = 1; width < bounds.Width; width++)
                {
                    int cost = (word1[height - 1] == word2[width - 1]) ? 0 : 1;
                    int insertion = matrix[height, width - 1] + 1;
                    int deletion = matrix[height - 1, width] + 1;
                    int substitution = matrix[height - 1, width - 1] + cost;

                    int distance = Math.Min(insertion, Math.Min(deletion, substitution));

                    if (height > 1 && width > 1 && word1[height - 1] == word2[width - 2] && word1[height - 2] == word2[width - 1])
                    {
                        distance = Math.Min(distance, matrix[height - 2, width - 2] + cost);
                    }

                    matrix[height, width] = distance;
                }
            }

            return matrix[bounds.Height - 1, bounds.Width - 1];
        }
    }
}