﻿using FuzzySearch.DAL;
using FuzzySearch.Models;
using FuzzySearch.Services.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuzzySearch.Services
{
    public class FuzzySearchImpl : IFuzzySearch
    {
        public FuzzySearchImpl()
        {
            basicFilter = new BasicSearchImpl();
            levenshteinDistanceFilter = new LevenshteinDistanceImpl();
            damerauLevenshteinDistanceFilter = new DamerauLevenshteinDistanceImpl();
        }

        public IEnumerable<DictionaryWord> Filter(IEnumerable<DictionaryWord> allWords, string word, int size)
        {
            //var filteredWords = basicFilter.Filter(allWords, word, size * 5);
            return levenshteinDistanceFilter.Filter(allWords, word, size);
            //return damerauLevenshteinDistanceFilter.Filter(allWords, word, size);
        }

        private IFuzzySearch basicFilter;
        private IFuzzySearch levenshteinDistanceFilter;
        private IFuzzySearch damerauLevenshteinDistanceFilter;
    }
}