﻿using Commons;
using FuzzySearch.Models;
using FuzzySearch.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FuzzySearch.Services
{
    public class LevenshteinDistanceImpl : IFuzzySearch
    {
        public IEnumerable<DictionaryWord> Filter(IEnumerable<DictionaryWord> allWords, string word, int size)
        {
            var priorityQueue = new PriorityQueue<int, DictionaryWord>(size);
            foreach (var item in allWords)
            {
                int distance;
                if (word.Length < item.Word.Length)
                {
                    int startPos = 0;
                    if (word.Length <= 5)
                    {
                        startPos = BitapSearchImpl.Search(item.Word, word, 2);
                    }

                    string word2 = (startPos > 0) ? item.Word.Substring(startPos) : item.Word;
                    distance = Math.Min(
                        HammingDistanceImpl.GetDistance(word, word2.Substring(0, word.Length))
                        , LevenshteinDistance(word, word2));
                }
                else
                {
                    distance = LevenshteinDistance(word, item.Word);
                }

                priorityQueue.EnQueue(int.MaxValue - distance, item);
            }

            return priorityQueue.GetAllValues();
        }

        private int LevenshteinDistance(string word1, string word2)
        {
            int n = word1.Length;
            int m = word2.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (word2[j - 1] == word1[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }
}