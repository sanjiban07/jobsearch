﻿using FuzzySearch.Models;
using System.Collections;
using System.Collections.Generic;

namespace FuzzySearch.Services.Interfaces
{
    public interface IFuzzySearch
    {
        IEnumerable<DictionaryWord> Filter(IEnumerable<DictionaryWord> allWords, string word, int size);
    }
}
