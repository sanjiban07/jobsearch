﻿namespace Tesco
{
    public interface IMasking
    {
        bool TryMask(string input, out string output);
    }
}
