﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tesco
{
    class MaskingFactory : IMasking
    {
        public MaskingFactory()
        {
            masks = new List<IMasking>();
        }

        public void AddMask(IMasking mask)
        {
            masks.Add(mask);
        }

        public bool TryMask(string input, out string output)
        {
            foreach (var mask in masks)
            {
                if (mask.TryMask(input, out output))
                {
                    return true;
                }
            }

            output = input;
            return false;
        }

        private readonly List<IMasking> masks;
    }
}
