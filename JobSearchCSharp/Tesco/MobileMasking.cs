﻿using System.Text.RegularExpressions;

namespace Tesco
{
    class MobileMasking : IMasking
    {
        public MobileMasking(string matchPattern, string convPattern, char replaceChar)
        {
            match = new Regex(matchPattern);
            conv = new Regex(convPattern);
            this.replaceChar = replaceChar;
        }

        public bool TryMask(string input, out string output)
        {
            output = input;
            if (match.IsMatch(input))
            {
                output = conv.Replace(input, new string(replaceChar, input.Length));
                return true;
            }

            return false;
        }

        private readonly Regex match;
        private readonly Regex conv;
        private readonly char replaceChar;
    }
}
