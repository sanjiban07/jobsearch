using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Tesco
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Config config = new Config()
            {
                SourceIds = new HashSet<int>() { 1 },
                BlockedKeys = new HashSet<string>() { "mobile" }
            };

            var logParser = new LogParser(config);
            logParser.AddMask(new MobileMasking(@"[\d]+", @"[\d]+", '0'));
            string output = logParser.Process(JsonConvert.SerializeObject(new LogObject()
            {
                SourceId = 1,
                Log = new Dictionary<string, string>()
                {
                    { "name", "sanjiban" },
                    { "mobile", "1234" },
                }
            }));

            Assert.IsFalse(output.Contains("1234"));
        }
    }
}
