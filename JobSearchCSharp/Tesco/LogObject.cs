﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Tesco
{
    class LogObject
    {
        public int SourceId;
        public Dictionary<string, string> Log;

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
