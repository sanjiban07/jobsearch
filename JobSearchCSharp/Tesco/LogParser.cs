﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tesco
{
    public class LogParser
    {
        public LogParser(Config config = null)
        {
            masking = new MaskingFactory();
            configs = new List<Config>();
            if (config != null)
            {
                configs.Add(config);
            }
        }

        public void AddConfig(Config config)
        {
            configs.Add(config);
        }

        public void AddMask(IMasking mask)
        {
            masking.AddMask(mask);
        }

        public string Process(string logLine)
        {
            LogObject logObj;
            try
            {
                logObj = JsonConvert.DeserializeObject<LogObject>(logLine);
            }
            catch (Exception)
            {
                return logLine;
            }

            foreach (var conf in configs)
            {
                if (conf.SourceIds.Contains(logObj.SourceId))
                {
                    foreach (var key in conf.BlockedKeys)
                    {
                        if (!masking.TryMask(logObj.Log[key], out string output))
                        {
                            output = new string('X', logObj.Log[key].Length);
                        }

                        logObj.Log[key] = output;
                    }
                }
            }

            return logObj.ToString();
        }

        private readonly MaskingFactory masking;
        private readonly List<Config> configs;
    }
}
