﻿using System.Collections.Generic;

namespace Tesco
{
    public class Config
    {
        public HashSet<int> SourceIds { get; set; }

        public HashSet<string> BlockedKeys { get; set; }
    }
}
